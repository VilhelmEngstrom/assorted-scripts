# Assorted Scripts
Miscellaneous smaller scripts I write to automate my systems.  

The repository currently includes
- [batmond](batmond) -- daemon monitoring battery level and warning when it falls below a chosen percentage
- [redshift\_toggle](redshift_toggle) -- toggles redshift on and off
- [todo](todo) -- recursively lists all TODOs in a directory
