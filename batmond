#!/bin/bash

#
# Battery monitor
#

sleep_interval=30 #s
empty_lvl=20 #%
full_lvl=-1  #%
notification_timeout=6000 #ms

# Check not already running
(( $(pgrep -c batmond) > 1 )) && exit

command -v notify-send >/dev/null || { echo libnotify required >&2; exit 1; }

function warn_full {
    [ $batstatus == discharging ] && return
    notify-send -t $notification_timeout --hint int:transient:1 --icon=battery-low "Battery full" "$batlvl%"
}

function warn_empty {
    [ $batstatus == charging ] && return
    notify-send -t $notification_timeout --hint int:transient:1 --icon=battery-caution "Low battery" "$batlvl%"
}

while true ; do
    batlvl=$(acpi | grep -oP "Battery\s*\d:[a-zA-Z, ]*\K\d+")
    batstatus=$(acpi | grep -oP "Battery\s*\d:\s*\K[a-zA-Z]+" | tr '[:upper:]' '[:lower:]')

    if (( $batlvl <= $empty_lvl )) ; then
        warn_empty
    elif (( $full_lvl > 0 && $batlvl >= $full_lvl )) ; then
        warn_full
    fi
    sleep $sleep_interval
done
